unit dblib.bitbuffer;

interface

{$I 'dblib.inc'}

{$DEFINE USE_ARRAYS}

uses
  System.SysUtils,
  System.Classes,
  dblib.common;

const
  CNT_BITBUFFER_SIGNATURE = $2109FAAD;

type

  // Exception classes
  EDBLibBitBuffer = class(Exception);

  (* About TDBLibBitBuffer:
    This class allows you to manage a large number of bits,
    much like TBits in VCL and LCL.
    However, it is not limited by the shortcomings of the initial TBits.

    - The bitbuffer can be saved
    - The bitbuffer can be loaded
    - The class exposes a linear memory model
    - The class expose methods (class functions) that allows you to
    perform operations on pre-allocated memory (memory you manage in
    your application).

    Uses of TDBLibBitbuffer:
    Bit-buffers are typically used to represent something else,
    like records in a database-file. A bit-map is often used in Db engines
    to represent what hapes are used (bit set to 1), and pages that can
    be re-cycled or compacted away later. *)

  {$IFDEF USE_ARRAYS}
  TByteAccessArray = array[0..MAXINT-1] of byte;
  PByteAccessArray = ^TByteAccessArray;
  {$ENDIF}

  TDBLibBitBuffer = class(TObject)
  private
    FData:      PByte;
    FDataLen:   cardinal;
    FBitsMax:   cardinal;
    FReadyByte: cardinal;
    function    GetByte(const Index: cardinal): byte; {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
    procedure   SetByte(const Index: cardinal; const Value: byte); {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}

    function    GetBit(const Index: cardinal): boolean; {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
    procedure   SetBit(const Index: cardinal; const Value: boolean); {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
  public
    property    Data: PByte read FData;
    property    Size: cardinal read FDataLen;
    property    Count: cardinal read FBitsMax;
    property    Bytes[const Index: cardinal]: byte Read GetByte write SetByte;
    property    Bits[const Index: cardinal]: boolean Read GetBit write SetBit; default;

    procedure   Allocate(MaxBits: cardinal);
    procedure   ReAllocate(NewMaxBits: cardinal);
    procedure   Release;
    function    Empty: boolean;
    procedure   Zero;

    function    ToStream: TStream;
    procedure   FromStream(const Stream: TStream; const Disposable: boolean = false);
    function    ToString(const Boundary: cardinal = 16): string; reintroduce;

    function    CalcFreeBits: cardinal; {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
    function    CalcSizeOfData: cardinal; {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}

    procedure   SaveToStream(const Stream: TStream); virtual;
    procedure   LoadFromStream(const Stream: TStream); virtual;

    procedure   SetBitRange(First, Last: cardinal; const Bitvalue: boolean); {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
    procedure   SetBits(const Value: TDbLibSequence; const Bitvalue: boolean); {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
    function    FindIdleBit(var Value: cardinal; const FromStart: boolean = false): boolean;

    constructor Create; virtual;
    destructor  Destroy; override;
  end;

implementation

uses
  dblib.partaccess,
  dblib.buffer;

// dont use resource-string! We use inline procedures for speed
resourcestring
  ERR_BitBuffer_InvalidBitIndex     = 'Invalid bit index, expected 0..%d not %d';
  ERR_BitBuffer_InvalidByteIndex    = 'Invalid byte index, expected 0..%d not %d';
  ERR_BitBuffer_BitBufferEmpty      = 'Bitbuffer is empty error';
  ERR_ERR_BitBuffer_INVALIDOFFSET   = 'Invalid bit offset, expected 0..%d, not %d';

//##########################################################################
//TDBLibBitBuffer
//##########################################################################

constructor TDBLibBitBuffer.Create;
begin
  inherited;
  FData := nil;
  FDataLen := 0;
  FBitsMax := 0;
  FReadyByte := 0;
end;

destructor TDBLibBitBuffer.Destroy;
Begin
  If not Empty then
    Release();

  inherited;
end;

function TDBLibBitBuffer.ToString(const Boundary: cardinal = 16): string;
const
  CNT_SYM: array [boolean] of string = ('0', '1');
var
  x: cardinal;
  LCount: cardinal;
begin
  lCount := Count;
  if lCount > 0 then
  begin
    lCount := TDbLibUtils.ToNearest(LCount, Boundary);
    x := 0;
    while x < lCount do
    begin
      if x < lCount then
      begin
        Result := Result + CNT_SYM[self[x]];
        if (x mod Boundary) = (Boundary - 1) then
          result := result + #13#10;
      end
      else
        result := result + '_';
      inc(x);
    end;
  end;
end;

function TDBLibBitBuffer.CalcFreeBits: cardinal;
var
  lAddr: pByte;
  llongs,
  lSingles: cardinal;
begin
  result := 0;

  if FData = nil then
    exit(0);

  lAddr := FData;

  llongs := FDataLen div 8;
  while llongs > 0 do
  begin
    inc(result, 8 - CNT_BitBuffer_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BitBuffer_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BitBuffer_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BitBuffer_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BitBuffer_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BitBuffer_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BitBuffer_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BitBuffer_ByteTable[lAddr^] ); inc(laddr);
    dec(llongs);
  end;

  lsingles := FDatalen mod 8;
  while lSingles > 0 do
  begin
    inc(result, 8 - CNT_BitBuffer_ByteTable[lAddr^] ); inc(laddr);
    dec(lSingles);
  end;
end;

function TDBLibBitBuffer.CalcSizeOfData: cardinal;
begin
  result := SizeOf(Cardinal) * 2;
  if FData <> nil then
    inc(result, FDataLen);
end;

function TDBLibBitBuffer.ToStream: TStream;
begin
  result := TMemoryStream.Create;
  SaveToStream(result);
  result.Position := 0;
end;

procedure TDBLibBitBuffer.FromStream(const Stream: TStream; const Disposable: boolean = false);
begin
  try
    LoadFromStream(Stream);
  finally
    if Disposable then
      Stream.Free;
  end;
end;

procedure TDBLibBitBuffer.SaveToStream(const Stream: TStream);
var
  LWriter: TWriter;
  LSign:  cardinal;
  lTemp: cardinal;
begin
  LWriter := TWriter.Create(stream, 1024);
  try
    // Write unique data signature
    LSign := CNT_BITBUFFER_SIGNATURE;
    LWriter.Write(LSign, SizeOf(LSign));

    // Write size of buffer in bytes
    lTemp := FDataLen;
    LWriter.Write(lTemp, SizeOf(lTemp));

    // Write buffer
    if FDataLen > 0 then
      LWriter.Write(FData^, FDataLen);
  finally
    // Flush to stream
    LWriter.FlushBuffer();
    LWriter.Free;
  end;
end;

procedure TDBLibBitBuffer.LoadFromStream(const stream: TStream);
var
  LReader:  TReader;
  LLen:     cardinal;
  LSign:    cardinal;
Begin
  if not Empty then
    Release();

  LReader := TReader.Create(stream, 1024 * 1024);
  try
    // Read uint32 signature
    LSign  := 0;
    LReader.Read(LSign, SizeOf(LSign));

    // Is this a bitbuffer?
    if LSign = CNT_BITBUFFER_SIGNATURE then
    begin
      // ok, read size of buffer in bytes
      LReader.Read(LLen, SizeOf(lLen) );

      if LLen > 0 then
      begin
        Allocate( TDbLibUtils.BitsOf(LLen) );
        LReader.Read(FData^, LLen);
      end;

    end else
      raise EDBLibBitBuffer.Create('Failed to load data, invalid data signature error');
  finally
    LReader.Free;
  end;
end;

Function TDBLibBitBuffer.Empty: boolean;
Begin
  Result := FData = NIL;
end;

Function TDBLibBitBuffer.GetByte(const Index: cardinal): byte;
{$IFNDEF USE_ARRAYS}
var
  lAddr: pByte;
{$ENDIF}
begin
  if FData = nil then
    raise EDBLibBitBuffer.Create('BitBuffer is empty error');

  if Index > FDataLen then
    raise EDBLibBitBuffer.CreateFmt('Invalid index [%d] expected 0..%d error', [Index, FDataLen-1]);

  {$IFDEF USE_ARRAYS}
  result := PByteAccessArray(FData)^[Index]
  {$ELSE}
  lAddr := FData;
  inc(lAddr, Index);
  result := lAddr^;
  {$ENDIF}
end;

procedure TDBLibBitBuffer.SetByte(const Index: cardinal; const Value: byte);
{$IFNDEF USE_ARRAYS}
var
  lAddr: pByte;
{$ENDIF}
begin
  if FData = nil then
    raise EDBLibBitBuffer.Create('BitBuffer is empty error');

  if Index > FDataLen then
    raise EDBLibBitBuffer.CreateFmt('Invalid index [%d] expected 0..%d error', [Index, FDataLen-1]);

  {$IFDEF USE_ARRAYS}
  PByteAccessArray(FData)^[Index] := value;
  {$ELSE}
  lAddr := FData;
  inc(lAddr, index);
  lAddr^ := value;
  {$ENDIF}
end;

procedure TDBLibBitBuffer.SetBitRange(First, Last: cardinal; const Bitvalue: boolean);
var
  x: cardinal;
  LLongs: cardinal;
  LSingles: cardinal;
  LCount: cardinal;

begin
  if FData = nil then
    raise EDBLibBitBuffer.Create('BitBuffer is empty error');

  if First < FBitsMax then
  Begin
    If Last < FBitsMax then
    begin
      // Swap first|last if not in right order
      If First > Last then
        TDbLibUtils.Swap(First, Last);

      // get totals, take ZERO into account
      LCount := TDbLibUtils.Diff(First, Last, true);

      // use refactoring & loop reduction
      LLongs := LCount div 8;

      x := First;

      while LLongs > 0 do
      Begin
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        dec(LLongs);
      end;

      (* process singles *)
      LSingles := LCount mod 8;
      while (LSingles > 0) do
      Begin
        SetBit(x, Bitvalue); inc(x);
        dec(LSingles);
      end;

    end else
    begin
      if First = Last then
        SetBit(First, true)
      else
        raise EDBLibBitBuffer.CreateFmt('Invalid bit index, expected 0..%d not %d', [FBitsMax-1, Last]);
    end;
  end else
  raise EDBLibBitBuffer.CreateFmt('Invalid bit index, expected 0..%d not %d', [FBitsMax-1, First]);
end;

procedure TDBLibBitBuffer.SetBits(Const Value: TDbLibSequence; const Bitvalue: boolean);
var
  llongs: integer;
  lSingles: integer;
  x: integer;
  lCount: integer;
begin
  if FData = nil then
    raise EDBLibBitBuffer.Create('Bitbuffer is empty error');

  lCount := length(Value);
  if lCount > 0 then
  begin
    x := low(Value);
    lLongs := lCount div 8;
    while (lLongs > 0) do
    begin
      SetBit(Value[x], Bitvalue); inc(x);
      SetBit(Value[x], Bitvalue); inc(x);
      SetBit(Value[x], Bitvalue); inc(x);
      SetBit(Value[x], Bitvalue); inc(x);
      SetBit(Value[x], Bitvalue); inc(x);
      SetBit(Value[x], Bitvalue); inc(x);
      SetBit(Value[x], Bitvalue); inc(x);
      SetBit(Value[x], Bitvalue); inc(x);
      dec(llongs);
    end;

    lSingles := lCount mod 8;
    while lSingles > 0 do
    begin
      SetBit(Value[x], Bitvalue); inc(x);
      dec(lSingles);
    end;
  end;
end;

function TDBLibBitBuffer.FindIdleBit(var Value: cardinal; const FromStart: boolean = false): boolean;
var
  lOffset: cardinal;
  lBit: cardinal;
  lAddr: PByte;
  x: cardinal;
Begin
  result := false;

  if FData = nil then
  begin
    Value := CNT_EOS;
    exit;
  end;

  // Initialize
  lAddr := FData;
  lOffset := 0;

  // Start from scratch or use ready-byte cursor?
  If FromStart then
    FReadyByte := 0
  else
  begin
    if FReadyByte > FDataLen then
      FReadyByte := 0
    else
    begin
      // If ready-byte at end of buffer, re-wind
      if FReadyByte = FDataLen then
      begin
        if GetByte(FReadyByte) = $FF then
          FReadyByte := 0;
      end;
    end;
  end;

  case (FReadyByte = 0) of
  true:
    begin
      // iterate through the buffer until we find a byte
      // that is not $FF, which means it has a free bit we can use
      While lOffset < FDataLen do
      Begin
        if lAddr^ < $FF then
        begin
          result := true;
          break;
        end;
        inc(lOffset);
        inc(lAddr);
      end;
    end;
  false:
    begin
      // Readybyte knows where to look, so just add the
      // index of the byte where bits are ready to the offset
      inc(lOffset, FReadyByte);
      inc(lAddr, FReadyByte);

      While lOffset < FDataLen do
      Begin
        if lAddr^ < $FF then
        begin
          result := true;
          break;
        end;
        inc(lOffset);
        inc(lAddr);
      end;

    end;
  end;

  // Did we indeed find a byte with a free bit?
  if result then
  begin
    // convert to bit index
    lBit := lOffset * 8;

    // Scan 8 iterations looking for the bit
    for x := 1 to 8 do
    Begin
      if GetBit(lBit) then
      begin
        inc(lBit);
        Continue;
      end;

      // This is the bit-index we found
      Value := lBit;

      if not (Value < FBitsMax) then
        raise EDBLibBitBuffer.Create('Internal error, index found beyond buffer boundary');

      // more than 1 bit available in byte? remember that
      FReadyByte := lOffset;
      break;
    end;

  end;
end;

Function TDBLibBitBuffer.GetBit(Const Index: cardinal): boolean;
var
  BitOfs: 0 .. 255;
  {$IFNDEF USE_ARRAYS}
  lAddr: pByte;
  lByteIndex: cardinal;
  {$ENDIF}
begin
  if FData = nil then
    raise EDBLibBitBuffer.Create('BitBuffer is empty error');

  if not (Index < FBitsMax) then
    raise EDBLibBitBuffer.CreateFmt('Index out of bounds, expected 0..%d not %d', [FBitsMax-1, Index]);

  BitOfs := Index mod 8;
  {$IFDEF USE_ARRAYS}
  result := (PByteAccessArray(FData)^[Index div 8] and (1 shl (BitOfs mod 8))) <> 0;
  {$ELSE}
  lAddr := FData;
  lByteIndex := cardinal(Index div 8);
  inc(lAddr, lByteIndex);
  result := lAddr^ and (1 shl (BitOfs mod 8)) <> 0;
  {$ENDIF}
end;

procedure TDBLibBitBuffer.SetBit(Const Index: cardinal; const Value: boolean);
var
  {$IFNDEF USE_ARRAYS}
  lAddr: pByte;
  {$ENDIF}
  lByte: byte;
  lByteIndex: cardinal;
  BitOfs: 0 .. 255;
begin
  if FData = nil then
    raise EDBLibBitBuffer.Create('BitBuffer is empty error');

  if Index > FBitsMax then
    raise EDBLibBitBuffer.CreateFmt('Index out of bounds, expected 0..%d not %d', [FBitsMax-1, Index]);

  BitOfs := Index mod 8;
  lByteIndex := Index div 8; //was div 8

  {$IFDEF USE_ARRAYS}
  lByte := PByteAccessArray(FData)^[lByteIndex];
  {$ELSE}
  lAddr := FData;
  inc(lAddr, lByteIndex);
  lByte := lAddr^;
  {$ENDIF}

  case Value of
  true:
    begin
      // set bit if not already set
      If (lByte and (1 shl (BitOfs mod 8))) = 0 then
      Begin
        lByte := (lByte or (1 shl (BitOfs mod 8)));
        {$IFDEF USE_ARRAYS}
        PByteAccessArray(FData)^[lByteIndex] := lByte;
        {$ELSE}
        lAddr^ := lByte;
        {$ENDIF}

        if (FReadyByte > 0) then
        begin
          if lByteIndex = FReadyByte then
          begin
            // No more free bits? Reset ready-byte
            if lByte = $FF then
              FReadyByte := 0
            else
            if TDbLibUtils.BitsSetInByte(lByte) > 7 then
              FReadyByte := 0;
          end;
        end;

      end;
    end;
  false:
    begin
      // clear bit if not already clear
      If (lByte and (1 shl (BitOfs mod 8))) <> 0 then
      Begin
        lByte := (lByte and not (1 shl (BitOfs mod 8)));
        {$IFDEF USE_ARRAYS}
        PByteAccessArray(FData)^[lByteIndex] := lByte;
        {$ELSE}
        lAddr^ := lByte;
        {$ENDIF}

        // remember this byte pos, because now we know that
        // there is at least 1 free bit there.
        // note: this stuff can be HEAVILY optimized, to make it
        // favour bytes with more available bits etc etc
        FReadyByte := lByteIndex;
      end;
    end;
  end;
end;

procedure TDBLibBitBuffer.ReAllocate(NewMaxBits: cardinal);
var
  lTemp:  pByte;
  lSize: cardinal;
  lOldSize: cardinal;
begin
  // Nothing allocated? Just do a clean alloc & exit
  if FData = nil then
  begin
    Allocate(NewMaxBits);
    exit;
  end;

  // keep current size
  lOldSize := FDataLen;

  // Get # of bytes. NOTE: This is affected by the growth scheme (!)
  // Do not expect to get exact bytes, it will round up by factorial
  lSize := TDbLibUtils.BytesOf(NewMaxBits);

  try
    lTemp := Allocmem(LSize);
  except
    on e: exception do
    raise EDBLibBitBuffer.Create('Failed to allocate new buffer error');
  end;

  try
    // zero out new memory
    fillchar(lTemp^, lSize, 0);

    // Copy existing bits over
    if lSize > FDataLen then
      move(FData^, LTemp^, FDataLen)
    else
      move(FData^, lTemp^, lSize);

  finally
    // Release old buffer
    FreeMem(FData);

    // new memory is now our buffer
    FData := lTemp;

    // If we are scaling up, then we want to place the lookup-cursor where
    // the old buffer ended, so that free bits are instantly available
    if lSize > lOldSize then
      FReadyByte := lOldSize-1
    else
      FReadyByte := 0;

    // Keep new buffer + info
    FDataLen := lSize;
    FBitsMax := TDbLibUtils.BitsOf(FDataLen);
  end;
end;

procedure TDBLibBitBuffer.Allocate(MaxBits: cardinal);
Begin
  (* release buffer if not empty *)
  If FData <> NIL then
    Release();

  If MaxBits > 0 then
  Begin
    (* Allocate new buffer *)
    try
      FReadyByte := 0;
      FDataLen := TDbLibUtils.BytesOf(MaxBits);
      FData := AllocMem(FDataLen);
      FBitsMax := FDatalen * 8;
    except
      on e: Exception do
      Begin
        FData := NIL;
        FDataLen := 0;
        FBitsMax := 0;
        raise;
      end;
    end;

  end;
end;

procedure TDBLibBitBuffer.Release;
Begin
  If FData <> NIL then
  Begin
    try
      FreeMem(FData);
    finally
      FReadyByte := 0;
      FData := NIL;
      FDataLen := 0;
      FBitsMax := 0;
    end;
  end;
end;

procedure TDBLibBitBuffer.Zero;
Begin
  If FData <> NIL then
    Fillchar(FData^, FDataLen, byte(0))
  else
    raise EDBLibBitBuffer.Create(ERR_BitBuffer_BitBufferEmpty);
end;

end.
