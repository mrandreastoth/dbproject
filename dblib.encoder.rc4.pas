unit dblib.encoder.rc4;

interface

{$I 'dblib.inc'}

uses
  System.SysUtils,
  System.classes,
  System.Math,
  System.Generics.Collections,
  System.Variants,
  dblib.common,
  dblib.buffer,
  dblib.encoder,
  dblib.encoder.buffer;

type

  TDbLibRC4EncodingTable = packed record
    etShr: packed array[0..255] of byte;
    etMod: packed array[0..255] of byte;
  end;

  TDbLibKeyRC4 = class(TDbLibEncodingKey)
  strict private
    FReady:   boolean;
    FTable:   TDbLibRC4EncodingTable;
  strict protected
    function  GetReady: boolean; override;
    procedure DoReset; override;
    procedure DoBuild(const Data; const ByteSize: integer); override;
    procedure DoBuild(const Data: TStream); override;
  public
    property  Table: TDbLibRC4EncodingTable read FTable;
  end;

  TDbLibEncoderRC4 = class(TDbLibEncoderBuffer)
  public
    function  EncodePtr(const Source; const Target; ByteLen: int64): boolean; override;
    function  EncodeStream(Source, Target: TStream): boolean; override;

    function  DecodePtr(const Source; const Target; ByteLen: int64): boolean; override;
    function  DecodeStream(Source, Target: TStream): boolean; override;
  end;

implementation

type
  TRCByteArray = packed array[0..4095] of byte;
  PRCByteArray = ^TRCByteArray;

//##########################################################################
// TDbLibKeyRC4
//##########################################################################

function TDbLibKeyRC4.GetReady: boolean;
begin
  result := FReady;
end;

procedure TDbLibKeyRC4.DoReset;
begin
  FReady := false;
  fillchar(FTable, SizeOf(FTable), #0);
end;

procedure TDbLibKeyRC4.DoBuild(const Data: TStream);
var
  FLen:   Integer;
  FData:  Pointer;
Begin
  if (Data <> NIL) and (Data.Size >= 256) then
  begin
    Data.Position:=0;
    FData := AllocMem(256);
    try
      fillchar(FData^,256,#0);
      FLen := Data.Read(FData^,256);
      if FLen > 0 then
        DoBuild(FData^, FLen);
    finally
      FreeMem(FData);
    end;
  end else
  DoReset();
end;

procedure TDbLibKeyRC4.DoBuild(const Data; const ByteSize: integer);
var
  i,j:    Integer;
  temp:   Byte;
  FData:  PRCByteArray;
begin
  (* reset key data *)
  DoReset();

  FData := @Data;

  if (FData <> NIL) and (ByteSize > 0) then
  begin
    J := 0;

    { Generate internal shift table based on key }
    for I:=0 to 255 do
    begin
      FTable.etShr[i] := i;
      If J = ByteSize then
        j := 1
      else
        inc(J);
      FTable.etMod[i] := FData[j-1];
    end;

    { Modulate shift table }
    J:=0;
    For i:=0 to 255 do
    begin
      j:=(j+FTable.etShr[i] + FTable.etMod[i]) mod 256;
      temp:=FTable.etShr[i];
      FTable.etShr[i]:=FTable.etShr[j];
      FTable.etShr[j]:=Temp;
    end;

    FReady := True;

  end else
  DoReset();
end;

//##########################################################################
// TDbLibEncoderRC4
//##########################################################################

function TDbLibEncoderRC4.EncodePtr(const Source; const Target; ByteLen: int64): boolean;
var
  i,j,t: integer;
  Temp,y:   byte;
  FSpare:   TDbLibRC4EncodingTable;
  LSource:  PByte;
  LTarget:  PByte;
  dx: int64;
begin
  LSource := @Source;
  LTarget := @Target;

  if  (GetReady()
  and (LSource <> nil)
  and (LTarget <> nil)
  and (Bytelen > 0) ) then
  begin
    // Fire begins event
    if assigned(OnEncodingBegins) then
    OnEncodingBegins(self, ByteLen);

    (* duplicate table *)
    FSpare := TDbLibKeyRC4(Key).Table;
    try
      i := 0;
      j := 0;
      dx := 0;
      while (dx < ByteLen) do
      begin
        i := (i + 1) mod 256;
        j := (j + FSpare.etShr[i]) mod 256;
        temp := FSpare.etShr[i];
        FSpare.etShr[i] := FSpare.etShr[j];
        FSpare.etShr[j] := temp;
        t := (FSpare.etShr[i] + (FSpare.etShr[j] mod 256)) mod 256;
        y := FSpare.etShr[t];

        // Fire progress event
        if assigned(OnEncodingProgress) then
        OnEncodingProgress(Self, dx, ByteLen);

        LTarget^ := Byte( LSource^ xor y );
        inc(LTarget);
        inc(LSource);
        dx := dx + 1;
      end;

      result := true;

      // Fire completion event
      if assigned(OnEncodingEnds) then
      OnEncodingEnds(Self, ByteLen);

    except
      on exception do
      result := false;
    end;
  end else
  result := false;
end;

function TDbLibEncoderRC4.EncodeStream(Source, Target: TStream): boolean;
var
  i,j,t:    integer;
  Temp,y:   byte;
  FDat:     byte;
  FSpare:   TDbLibRC4EncodingTable;
  dx:       int64;
  ByteLen:  int64;
begin
  result := false;

  if  (GetReady()
  and (Source <> nil)
  and (Target <> nil)
  and (Source.Size > 0) ) then
  begin
    // rewind stream?
    if Source.Position <> 0 then
    Source.Position := 0;

    // grab the size
    ByteLen := Source.Size;

    (* duplicate table *)
    FSpare := TDbLibKeyRC4(Key).Table;

    try
      // Fire begins event
      if assigned(OnEncodingBegins) then
      OnEncodingBegins(self, ByteLen);

      i:=0;
      j:=0;
      dx := 0;
      while dx < Source.Size do
      Begin
        i := (i + 1) mod 256;
        j := (j + FSpare.etShr[i]) mod 256;
        temp := FSpare.etShr[i];
        FSpare.etShr[i] := FSpare.etShr[j];
        FSpare.etShr[j] := temp;
        t := (FSpare.etShr[i] + (FSpare.etShr[j] mod 256)) mod 256;
        y := FSpare.etShr[t];

        // Fire progress event
        if assigned(OnEncodingProgress) then
        OnEncodingProgress(Self, dx, ByteLen);

        if source.Read(FDat, SizeOf(FDat)) = SizeOf(FDat) then
        Begin
          FDat := FDat xor y;
          if Target.Write(FDat, SizeOf(FDat)) <> SizeOf(FDat) then
          exit;
        end else
        exit;
        dx := dx + 1;
      end;

      result := true;

      // Fire completion event
      if assigned(OnEncodingEnds) then
      OnEncodingEnds(Self, ByteLen);

    except
      on exception do
      result := false;
    end;
  end;
end;

function TDbLibEncoderRC4.DecodePtr(const Source; const Target; ByteLen: int64): boolean;
var
  i,j,t: integer;
  Temp,y:   byte;
  FSpare:   TDbLibRC4EncodingTable;
  LSource:  PByte;
  LTarget:  PByte;
  dx: int64;
begin
  LSource := @Source;
  LTarget := @Target;

  if  (GetReady()
  and (LSource <> nil)
  and (LTarget <> nil)
  and (Bytelen > 0) ) then
  begin
    // Fire begins event
    if assigned(OnDecodingBegins) then
    OnDecodingBegins(self, ByteLen);

    (* duplicate table *)
    FSpare := TDbLibKeyRC4(Key).Table;
    try
      i := 0;
      j := 0;
      dx := 0;
      while (dx < ByteLen) do
      begin
        i := (i + 1) mod 256;
        j := (j + FSpare.etShr[i]) mod 256;
        temp := FSpare.etShr[i];
        FSpare.etShr[i] := FSpare.etShr[j];
        FSpare.etShr[j] := temp;
        t := (FSpare.etShr[i] + (FSpare.etShr[j] mod 256)) mod 256;
        y := FSpare.etShr[t];

        // Fire progress event
        if assigned(OnDecodingProgress) then
        OnDecodingProgress(Self, dx, ByteLen);

        LTarget^ := Byte( LSource^ xor y );
        inc(LTarget);
        inc(LSource);
        dx := dx + 1;
      end;

      result := true;

      // Fire completion event
      if assigned(OnDecodingEnds) then
      OnDecodingEnds(Self, ByteLen);

    except
      on exception do
      result := false;
    end;
  end else
  result := false;
end;

function TDbLibEncoderRC4.DecodeStream(Source, Target: TStream): boolean;
var
  i,j,t:    integer;
  Temp,y:   byte;
  FDat:     byte;
  FSpare:   TDbLibRC4EncodingTable;
  dx:       int64;
  ByteLen:  int64;
begin
  result := false;

  if  (GetReady()
  and (Source <> nil)
  and (Target <> nil)
  and (Source.Size > 0) ) then
  begin
    // rewind stream?
    if Source.Position <> 0 then
    Source.Position := 0;

    // grab the size
    ByteLen := Source.Size;

    (* duplicate table *)
    FSpare := TDbLibKeyRC4(Key).Table;

    try
      // Fire begins event
      if assigned(OnDecodingBegins) then
      OnDecodingBegins(self, ByteLen);

      i:=0;
      j:=0;
      dx := 0;
      while dx < Source.Size do
      Begin
        i := (i + 1) mod 256;
        j := (j + FSpare.etShr[i]) mod 256;
        temp := FSpare.etShr[i];
        FSpare.etShr[i] := FSpare.etShr[j];
        FSpare.etShr[j] := temp;
        t := (FSpare.etShr[i] + (FSpare.etShr[j] mod 256)) mod 256;
        y := FSpare.etShr[t];

        // Fire progress event
        if assigned(OnDecodingProgress) then
        OnDecodingProgress(Self, dx, ByteLen);

        if source.Read(FDat, SizeOf(FDat)) = SizeOf(FDat) then
        Begin
          FDat := FDat xor y;
          if Target.Write(FDat, SizeOf(FDat)) <> SizeOf(FDat) then
          exit;
        end else
        exit;
        dx := dx + 1;
      end;

      result := true;

      // Fire completion event
      if assigned(OnDecodingEnds) then
      OnDecodingEnds(Self, ByteLen);

    except
      on exception do
      result := false;
    end;
  end;
end;



end.
