object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Scatter engine testbed by Jon L. Aasenden'
  ClientHeight = 382
  ClientWidth = 561
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object memoOut: TMemo
    AlignWithMargins = True
    Left = 3
    Top = 60
    Width = 555
    Height = 319
    Align = alClient
    Lines.Strings = (
      'Click the button to perform read/write proof test')
    ScrollBars = ssVertical
    TabOrder = 0
    WantTabs = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 561
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object btnWriteReadTest: TButton
      Left = 8
      Top = 8
      Width = 177
      Height = 41
      Caption = 'Create / Open DB'
      TabOrder = 0
      OnClick = btnWriteReadTestClick
    end
  end
end
