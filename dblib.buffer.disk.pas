unit dblib.buffer.disk;

interface

{$I 'dblib.inc'}

{$DEFINE USE_SEEK}

uses
  System.SysUtils,
  System.classes,
  System.Math,
  System.Generics.Collections,
  System.Variants,
  dblib.common,
  dblib.buffer,
  dblib.encoder;


type
  //  This class implements a file based buffer.
  //  It allows you to allocate and manipulate a file with the same simplicity
  //  as you would a TFileStream, but with all the added benefits of the
  //  buffer-api, like insertion and removal of chunks at any location

  // Note: this should be re-written to use old-school AssignFile etc

  TDbLibBufferFile = class(TDbLibBuffer)
  strict private
    FFilename:  string;
    FFile:      TStream;
  strict protected
    function    GetActive: boolean;
    function    GetEmpty: boolean; override;

    function    DoGetCapabilities: TDbLibBufferCapabilities; override;
    function    DoGetDataSize: Int64; override;
    procedure   DoReleaseData; override;
    procedure   DoReadData(Start: Int64; var Buffer; BufLen: integer); override;
    procedure   DoWriteData(Start: Int64;const Buffer; BufLen:integer); override;
    procedure   DoGrowDataBy(const Value:integer); override;
    procedure   DoShrinkDataBy(const Value:integer); override;
  public
    property    Filename: string read FFilename;
    property    Active: boolean read GetActive;
    procedure   Open(Filename: string; StreamFlags: Word); virtual;
    procedure   Close; virtual;

    procedure   BeforeDestruction; override;
    constructor CreateEx(Filename: string; StreamFlags: Word); overload;
  end;

implementation


//##########################################################################
// TDbLibBufferFile
//##########################################################################

constructor TDbLibBufferFile.CreateEx(Filename: string; StreamFlags: Word);
begin
  inherited Create();

  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  Open(Filename, StreamFlags);

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['CreateEx',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferFile.BeforeDestruction;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if FFile <> nil then
  begin
    FreeAndNIL(FFile);
    FFilename := '';
  end;

  inherited;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['BeforeDestruction',e.classname,e.message]);
  end;
  {$ENDIF}
end;

function TDbLibBufferFile.DoGetCapabilities: TDbLibBufferCapabilities;
begin
  result := [mcScale, mcOwned, mcRead, mcWrite];
end;

function TDbLibBufferFile.GetEmpty: boolean;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if FFile <> nil then
    result := (FFile.Size = 0)
  else
    result := true;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['GetEmpty',e.classname,e.message]);
  end;
  {$ENDIF}
end;

function TDbLibBufferFile.GetActive: boolean;
begin
  result := FFile <> nil;
end;

procedure TDbLibBufferFile.Open(Filename: string; StreamFlags: Word);
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  if FFile <> nil then
    Close();

  try
    FFile := TFileStream.Create(Filename, StreamFlags);
  except
    on e: exception do
      raise EDbLibBufferError.CreateFmt
      ('Failed to create filestream (%s), system threw exception %s with message %s',
      [Filename, e.ClassName, e.message]);
  end;

  FFileName := Filename;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Open',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferFile.Close;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if FFile <> nil then
  begin
    FreeAndNIL(FFile);
    FFilename := '';
  end;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Close',e.classname,e.message]);
  end;
  {$ENDIF}
end;

function TDbLibBufferFile.DoGetDataSize: Int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if FFile <> nil then
    result := FFile.Size
  else
    result := 0;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoGetDataSize',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferFile.DoReleaseData;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if FFile <> nil then
    FFile.Size := 0;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoReleaseData',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferFile.DoReadData(Start: Int64; var Buffer; BufLen: integer);
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if FFile <> nil then
  begin
    if FFile.Position <> Start then
    {$IFDEF USE_SEEK}
    FFile.Seek32(Start, TSeekOrigin.soBeginning);
    {$ELSE}
    FFile.Position := Start;
    {$ENDIF}

    FFile.ReadBuffer(Buffer, BufLen);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_NOTACTIVE);

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoReadData', e.classname, e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferFile.DoWriteData(Start: Int64; const Buffer; BufLen: integer);
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if FFile <> nil then
  begin
    if FFile.Position <> Start then
    {$IFDEF USE_SEEK}
    FFile.Seek32(Start, TSeekOrigin.soBeginning);
    {$ELSE}
    FFile.Position := Start;
    {$ENDIF}
    FFile.WriteBuffer(Buffer, BufLen);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_NOTACTIVE);

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoWriteData',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferFile.DoGrowDataBy(const Value: integer);
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if FFile <> nil then
    FFile.Size := FFile.Size + Value
  else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_NOTACTIVE);

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoGrowDataBy',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferFile.DoShrinkDataBy(const Value: integer);
var
  LNewSize: Int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if FFile <> nil then
  begin
    LNewSize := FFile.Size - Value;
    if LNewSize > 0 then
      FFile.Size := LNewSize
    else
      DoReleaseData();
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_NOTACTIVE);

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoShrinkDataBy',e.classname,e.message]);
  end;
  {$ENDIF}
end;

end.
