unit dblib.partaccess;

interface

{$I 'dblib.inc'}

uses
  System.SysUtils,
  System.classes,
  System.Math,
  System.Generics.Collections,
  System.Variants,
  dblib.common,
  dblib.buffer,
  dblib.encoder,
  dblib.readwrite,
  dblib.records,
  dblib.encoder.buffer;

const
  CNT_DATABASEFILE_SIGNATURE = cardinal($CAFEBABE);
  CNT_DATABASEPART_SIGNATURE = cardinal($BAADBABE);
  CNT_DATABARERECL_SIGNATURE = cardinal($FAABCAFE);

  CNT_DATABASEFILE_MAJOR = 0;
  CNT_DATABASEFILE_MINOR = 0;
  CNT_DATABASEFILE_REVISION = 1;
  CNT_DATABASEFILE_PAGESIZE = 1024;
  CNT_EOS = $FFFFFFFF;

type

  EDblibPartAccess = class(EDbLibError);


  // Custom data structures
  TDbVersion = packed record
    bvMajor: byte;
    bvMinor: byte;
    bvRevision: word;
  end;

  TDbLibFileHeader = packed record
    dhSignature:  cardinal;
    dhVersion:    TDbVersion;
    dhName:       shortstring;
    dhMetadata:   cardinal;
    dhRecList:    cardinal;
    dhBitBuffer:  cardinal;
    class function Create: TDbLibFileHeader; static;
  end;

  TDbPartData = packed record
    ddSignature: cardinal;
    ddRoot: cardinal;
    ddPrevious: cardinal;
    ddNext: cardinal;
    ddBytes: cardinal;
    ddData: packed array [0 .. CNT_DATABASEFILE_PAGESIZE - 1] of byte;

    function  DataToBuffer(const Buffer: TDbLibBuffer): cardinal;
    function  BufferToData(const Buffer: TDbLibBuffer): cardinal;

    function  DataToStream(const Stream: TStream): cardinal;
    function  StreamToData(const Stream: TStream): cardinal;

    class function Create: TDbPartData; static;
    procedure Reset;
  end;

  TDbLibPartAccess = class(TObject)
  private
    FBuffer:    TDbLibBuffer;
    FheadSize:  cardinal;
    FPartSize:  cardinal;
    FPageSize:  cardinal;
    FCache:     PByte;
  protected
    function    GetPartCount: cardinal; inline;
  public
    property    Buffer: TDbLibBuffer read FBuffer;
    property    ReservedHeaderSize: cardinal read FheadSize;
    property    PartSize: cardinal read FPartSize;
    property    PageSize: cardinal read FPageSize;
    property    PartCount: cardinal read GetPartCount;

    procedure   ReadPart(const PartIndex: cardinal; var Part: TDbPartData); inline;
    procedure   WritePart(const PartIndex: cardinal; const Part: TDbPartData ); inline;
    procedure   AppendPart(const Part: TDbPartData); inline;

    procedure   ZeroPart(const PartIndex: cardinal); inline;
    procedure   ZeroSequence(Sequence: TDbLibSequence); inline;

    function    CalcPartsForData(const DataSize: Int64): cardinal; inline;
    function    CalcOffsetForPart(const PartIndex: cardinal): Int64; inline;

    constructor Create(const DataBuffer: TDbLibBuffer;
                const ReservedHeaderSize: cardinal;
                const PartSize, PageSize: cardinal); reintroduce; virtual;

    destructor  Destroy; override;
  end;


implementation

//#############################################################################
// TDbLibFileHeader
//#############################################################################

class function TDbLibFileHeader.Create: TDbLibFileHeader;
begin
  result.dhSignature := CNT_DATABASEFILE_SIGNATURE;
  result.dhVersion.bvMajor := CNT_DATABASEFILE_MAJOR;
  result.dhVersion.bvMinor := CNT_DATABASEFILE_MINOR;
  result.dhVersion.bvRevision := CNT_DATABASEFILE_REVISION;
  result.dhName := '';
  result.dhMetadata := CNT_EOS;
  result.dhRecList := CNT_EOS;
end;

//#############################################################################
// TDbPartData
//#############################################################################

class function TDbPartData.Create: TDbPartData;
begin
  result.Reset();
end;

procedure TDbPartData.Reset;
begin
  self.ddSignature := CNT_DATABASEPART_SIGNATURE;
  self.ddRoot := CNT_EOS;
  self.ddPrevious := CNT_EOS;
  self.ddNext := CNT_EOS;
  self.ddBytes := 0;
  fillchar(ddData, SizeOf(ddData), byte(0) );
end;

function TDbPartData.DataToBuffer(const Buffer: TDbLibBuffer): cardinal;
begin
  result := 0;
  if self.ddSignature = CNT_DATABASEPART_SIGNATURE then
  begin
    if Buffer <> nil then
    begin
      if self.ddBytes > 0 then
        buffer.Push(self.ddData, SizeOf(self.ddData), TDbLibStackOrientation.soDownUp);
        //buffer.Append(self.ddData, self.ddBytes);
    end else
    raise Exception.Create('Invalid target buffer, failed to get page data');
  end else
  raise Exception.Create('Invalid signature error');
end;

function TDbPartData.BufferToData(const Buffer: TDbLibBuffer): cardinal;
begin
  fillchar(self.ddData, SizeOf(self.ddData), byte(0) );
  if Buffer <> nil then
  begin
    self.ddBytes := buffer.Pull(self.ddData, SizeOf(self.ddData), TDbLibStackOrientation.soDownUp);
    //self.ddBytes := Buffer.Read(0, SizeOf(self.ddData), self.ddData);
    result := self.ddBytes;
  end else
  raise Exception.Create('Invalid source buffer, failed to get page data');
end;

function TDbPartData.DataToStream(const Stream: TStream): cardinal;
begin
  result := 0;
  if self.ddSignature = CNT_DATABASEPART_SIGNATURE then
  begin
    if Stream <> nil then
    begin
      if self.ddBytes > 0 then
        result := Stream.Write(self.ddData, self.ddBytes);
    end else
    raise Exception.Create('Invalid target stream, failed to get page data');
  end else
  raise Exception.Create('Invalid signature error');
end;

function TDbPartData.StreamToData(const Stream: TStream): cardinal;
begin
  fillchar(self.ddData, SizeOf(self.ddData), byte(0) );
  if Stream <> nil then
  begin
    self.ddBytes := Stream.read(self.ddData, SizeOf(self.ddData));
    result := self.ddBytes;
  end else
  raise Exception.Create('Invalid source stream, failed to get page data');
end;


// ############################################################################
// TDbLibPartAccess
// ############################################################################

constructor TDbLibPartAccess.Create(const DataBuffer: TDbLibBuffer;
      const ReservedHeaderSize: cardinal; const PartSize, PageSize: cardinal);
begin
  inherited Create;

  // Make sure buffer is valid
  if DataBuffer <> nil then
    FBuffer := DataBuffer
  else
    raise EDblibPartAccess.Create('Buffer cannot be NIL error');

  // Keep track of header size, we even allow no header to accepted
  FHeadSize := ReservedHeaderSize;

  // Validate the part size, this has to be a valid, positive number
  if PartSize > 0 then
    FPartSize := PartSize
  else
    raise EDblibPartAccess.Create('Invalid partsize error');

  if PageSize > 0 then
    FPageSize := PageSize
  else
    raise EDbLibPartAccess.Create('Invalid pagesize error');

  // Allocate our read/write cache
  FCache := Allocmem(FPartSize);
end;

destructor TDbLibPartAccess.Destroy;
begin
  // release read/write cache
  Freemem(FCache);
  inherited;
end;

function TDbLibPartAccess.CalcPartsForData(const DataSize: Int64): cardinal;
begin
  // Note: We cannot use part-size here since the partsize represents
  //       the whole block, including extra info. Each block has a field
  //       for holding data, which is what we must send in the pagesize
  //       parameter via the constructor
  if DataSize < 1 then
    exit(0);

  if DataSize > FPageSize then
  begin
    result := DataSize div FPageSize;
    if (result * FPageSize) < DataSize then
      inc(result);
  end else
    result := 1;
end;

function TDbLibPartAccess.CalcOffsetForPart(const PartIndex: cardinal): Int64;
begin
  result := PartIndex * FPartSize;
  result := result + FHeadSize;
end;

function TDbLibPartAccess.GetPartCount: cardinal;
var
  lTotal: Int64;
begin
  if FBuffer.Size < 1 then
    exit(0);

  lTotal := FBuffer.Size;
  lTotal := lTotal - FHeadSize;

  result := lTotal div FPartSize;
end;

procedure TDbLibPartAccess.ReadPart(const PartIndex: cardinal; var Part: TDbPartData);
var
  lOffset: int64;
begin
  // Make sure we reset the part no matter what
  Part.Reset();

  // make sure we have read access
  if not (mcRead in FBuffer.Capabilities) then
    raise EDblibPartAccess.Create(CNT_ERR_BTRG_READNOTSUPPORTED);

  // Make sure we dont read beyond file
  if PartIndex >= GetPartCount() then
    raise EDblibPartAccess.CreateFmt('Invalid index, expected part 0..%d not %d', [GetPartCount()-1, PartIndex]);

  lOffset := CalcOffsetForPart(PartIndex);
  FBuffer.Read(lOffset, FPartSize, Part);
end;


procedure TDbLibPartAccess.AppendPart(const Part: TDbPartData);
begin
  // Check that our buffer has write access
  if not (mcWrite in FBuffer.Capabilities) then
    raise EDblibPartAccess.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);

  // Check that our buffer allows scaling
  if not (mcScale in FBuffer.Capabilities) then
    raise EDblibPartAccess.Create(CNT_ERR_BTRG_SCALENOTSUPPORTED);

  FBuffer.Append(Part, SizeOf(Part));
end;

procedure TDbLibPartAccess.ZeroSequence(Sequence: TDbLibSequence);
var
  lIndex: cardinal;
  lCount: integer;
begin
  lCount := length(Sequence);
  if lCount > 0 then
  begin
    lIndex := low(Sequence);
    while lCount > 0 do
    begin
      ZeroPart(Sequence[lIndex]);
      inc(lIndex);
      dec(lCount);
    end;
  end;
end;

procedure TDbLibPartAccess.ZeroPart(const PartIndex: cardinal);
var
  lPart:    TDbPartData;
  lOffset:  int64;
begin
  // Check that our buffer has write access
  if not (mcWrite in FBuffer.Capabilities) then
    raise EDblibPartAccess.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);

  // Make sure we dont write beyond file
  if PartIndex >= GetPartCount() then
    raise EDblibPartAccess.CreateFmt('Invalid index, expected part 0..%d not %d', [GetPartCount()-1, PartIndex]);

  lPart := TDbPartData.Create();
  lOffset := CalcOffsetForPart(PartIndex);
  FBuffer.Write(lOffset, SizeOf(lPart), lPart);
end;

procedure TDbLibPartAccess.WritePart(const PartIndex: cardinal; const Part: TDbPartData);
var
  lOffset: int64;
begin
  // Check that our buffer has write access
  if not (mcWrite in FBuffer.Capabilities) then
    raise EDblibPartAccess.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);

  lOffset := CalcOffsetForPart(PartIndex);
  FBuffer.Write(lOffset, SizeOf(Part), Part);
end;

end.
